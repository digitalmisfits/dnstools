package com.avast.dns.dga.features;

import com.google.common.collect.Lists;
import org.junit.Test;

import java.util.Map;
import java.util.stream.Stream;

public class NGramFrequencyTest {

    @Test
    public void avg() throws Exception {

        final Map<String, Double> freq = NGramFrequency.bi(Lists.newArrayList(
                "google"
        ));

        System.out.println(NGramFrequency.avg(freq, "google"));
    }

    @Test
    public void bi() throws Exception {

        final Map<String, Double> words = NGramFrequency.bi(Lists.newArrayList(
                "google",
                "facebook",
                "linkedin",
                "microsoft"
        ));

//        Assert.assertEquals(2, words.get("oo"), 0);
//        Assert.assertEquals(1, words.get("ce"), 0);
//        Assert.assertEquals(1, words.get("ac"), 0);

        System.out.println(words);
    }
}
