package com.avast.dns.dga.features;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ShannonEntropyTest {

    @Test
    public void entropy() throws Exception {
        assertEquals(2.8423709931771084, ShannonEntropy.entropy("www.google.com"), 0);
    }
}
