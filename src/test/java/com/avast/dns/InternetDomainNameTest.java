package com.avast.dns;

import com.google.common.net.InternetDomainName;
import org.junit.Test;

public class InternetDomainNameTest {


    @Test
    public void testPrivateDomain() {


        final InternetDomainName idn
                = InternetDomainName.from("www.google.co.uk");

        final InternetDomainName topPrivateDomain = idn.topPrivateDomain();
        final InternetDomainName tld = idn.registrySuffix();


        System.out.println(idn);
        System.out.println(topPrivateDomain);
        System.out.println(tld);

    }
}
