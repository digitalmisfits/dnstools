package com.avast.dns.ml;

import com.avast.dns.ml.arff.*;
import com.google.common.collect.Lists;
import org.junit.Assert;
import org.junit.Test;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ArffWriterTest {

    private enum TestClasses {
        CLASS_A,
        CLASS_B,
        CLASS_C,
        CLASS_D
    }

    @Test
    public void write() throws Exception {

        final StringWriter writer = new StringWriter();

        final RelationHeader relationHeader = new RelationHeader("junit");
        final List<AttributeHeader> attributeHeaders = Lists.newArrayList(
                new RealAttributeHeader("attribute1"),
                new RealAttributeHeader("attribute2"),
                new NominalAttributeHeader<>("class", TestClasses.class));

        final List<Attribute> attributes = Lists.newArrayList(
                new RealAttribute(2.0f),
                new RealAttribute(2.0f),
                new NominalAttribute<>(TestClasses.CLASS_A)
        );

        final List<List<Attribute>> instances = new ArrayList<>();
        instances.add(attributes);
        instances.add(attributes);

        final ArffWriter arff = new ArffWriter(writer);
        arff.write(relationHeader, attributeHeaders, instances);


        final List<String> result
                = Arrays.asList(writer.toString().split("\n"));

        Assert.assertEquals(result.get(0), "@RELATION junit");
        Assert.assertEquals(result.get(1), "@ATTRIBUTE attribute1 NUMERIC");
        Assert.assertEquals(result.get(2), "@ATTRIBUTE attribute2 NUMERIC");
        Assert.assertEquals(result.get(3), "@ATTRIBUTE class { CLASS_REGULAR, CLASS_DGA, CLASS_C, CLASS_D }");
        Assert.assertEquals(result.get(4), "@DATA");
        Assert.assertEquals(result.get(5), "2.00,2.00,CLASS_REGULAR");
        Assert.assertEquals(result.get(6), "2.00,2.00,CLASS_REGULAR");

        System.out.println(writer.toString());
    }
}

