package com.avast.dns;

import com.google.common.collect.Lists;
import weka.classifiers.Classifier;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Verifier {

    public static void main(String[] args) throws Exception {

        final ClassLoader cl = Trainer.class.getClassLoader();

        // Load model
        final Classifier classifier
                = (Classifier) weka.core.SerializationHelper.read(cl.getResourceAsStream("weka/j48.model"));

        final Attribute freqAttribute = new Attribute("freq");
        final Attribute shannonAttribute = new Attribute("shannon");
        final List<String> classes
                = Lists.newArrayList("CLASS_REGULAR", "CLASS_DGA");

        final ArrayList<Attribute> attributeList = new ArrayList<Attribute>(2) {
            {
                add(freqAttribute);
                add(shannonAttribute);

                final Attribute attributeClass = new Attribute("@@class@@", classes);
                add(attributeClass);
            }
        };

        final Instances unpredicted = new Instances("dga", attributeList, 1);
        unpredicted.setClassIndex(unpredicted.numAttributes() - 1);

        final Instance instance = new DenseInstance(2);
        instance.setValue(0, 1.1115);
        instance.setValue(1, 3.5778);
        instance.setDataset(unpredicted);

        System.out.println(Arrays.toString(classifier.distributionForInstance(instance)));
    }
}
