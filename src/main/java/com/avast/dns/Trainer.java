package com.avast.dns;

import com.avast.dns.dga.features.NGramFrequency;
import com.avast.dns.dga.features.ShannonEntropy;
import com.avast.dns.ml.ArffWriter;
import com.avast.dns.ml.arff.*;
import com.google.common.collect.Lists;
import com.google.common.net.InetAddresses;
import com.google.common.net.InternetDomainName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Trainer {

    private static Logger LOGGER = LoggerFactory.getLogger(Trainer.class);

    public static void main(String[] args) throws IOException {

        final ClassLoader cl = Trainer.class.getClassLoader();

        LOGGER.info("Training n-grams.");

        // Train frequency
        final Map<String, Double> frequency;

        try (BufferedReader reader =
                     new BufferedReader(new InputStreamReader(cl.getResourceAsStream("majestic-1m.txt")))) {



            frequency = NGramFrequency.bi(reader.lines()
                    .parallel()
                    .filter(s -> !s.startsWith("xn--"))
                    .collect(Collectors.toList()));

        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        LOGGER.info("Finished training n-grams.");

        LOGGER.info("Calculating normal instances.");

        // calculate normal instances
        final List<List<Attribute>> normalInstances;

        try (BufferedReader reader =
                     new BufferedReader(new InputStreamReader(cl.getResourceAsStream("majestic-1m.txt")))) {

            normalInstances = reader.lines()
                    .parallel()
                    .filter(s -> !s.startsWith("xn--"))
                    .map(s -> {
                        return Lists.newArrayList(
//                                new RealAttribute(1.0),
                                new RealAttribute(NGramFrequency.avg(frequency, s)),
                                new RealAttribute(ShannonEntropy.entropy(s)),
                                new NominalAttribute<>(DomainClass.CLASS_REGULAR)
                        );
                    })
                    .collect(Collectors.toList());


        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        LOGGER.info("Finished calculating normal instances.");

        LOGGER.info("Starting calculating dga instances.");

        // calculate normal instances
        final List<List<Attribute>> dgaInstances;

        try (BufferedReader reader =
                     new BufferedReader(new InputStreamReader(cl.getResourceAsStream("dga-100k.txt")))) {

            dgaInstances = reader.lines()
                    .parallel()
                    .map(s -> {
                        return Lists.newArrayList(
//                                new RealAttribute(0.0),
                                new RealAttribute(NGramFrequency.avg(frequency, s)),
                                new RealAttribute(ShannonEntropy.entropy(s)),
                                new NominalAttribute<>(DomainClass.CLASS_DGA)
                        );
                    })
                    .collect(Collectors.toList());

        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        LOGGER.info("Finished calculating dga instances.");

        LOGGER.info("Writing weka arff file");

        final BufferedWriter writer = new BufferedWriter(new FileWriter("out.arff"));

        final List<AttributeHeader> headers = Lists.newArrayList(
//                new RealAttributeHeader("resolvable"),
                new RealAttributeHeader("freq"),
                new RealAttributeHeader("shannon"),
                new NominalAttributeHeader<>("class", DomainClass.class)
        );

        final List<List<Attribute>> instances = Stream
                .concat(normalInstances.stream(), dgaInstances.stream())
                .collect(Collectors.toList());

        final ArffWriter arff = new ArffWriter(writer);
        arff.write(new RelationHeader("weka"), headers, instances);
        arff.close();

        LOGGER.info("Finished weka arff file");
    }

}
