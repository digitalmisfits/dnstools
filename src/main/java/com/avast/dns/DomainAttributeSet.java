package com.avast.dns;

import com.avast.dns.ml.arff.Attribute;
import com.google.common.collect.ImmutableList;

import java.util.Arrays;
import java.util.List;

public class DomainAttributeSet {

    private final List<Attribute> attributes;

    public DomainAttributeSet(Attribute... attributes) {
        this.attributes = Arrays.asList(attributes);
    }

    public List<Attribute> attributes() {
        return ImmutableList.copyOf(attributes);
    }
}
