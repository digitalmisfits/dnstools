package com.avast.dns.dga.features;

import java.util.*;
import java.util.AbstractMap.SimpleEntry;
import java.util.stream.Collectors;

public class NGramFrequency {

    public static double avg(final Map<String, Double> lookup, final String s) {
        final int ngrams = 2;

        final List<Double> total = new ArrayList<>();
        for (int i = 0; i < s.length() - 1; i++) {
            total.add(lookup.getOrDefault(s.substring(i, i + ngrams), 0d));
        }
        return total.stream()
                .mapToDouble(v -> v)
                .average()
                .orElseThrow(() -> new RuntimeException("Failed to calculate average. NaN"));
    }

    public static Map<String, Double> bi(final List<String> input) {
        final int ngrams = 2;

        final Map<String, Integer> freq = new HashMap<>();
        input.forEach(s -> {
            for (int i = 0; i < s.length() - 1; i++) {
                final String ngram = s.substring(i, i + ngrams);
                if (freq.containsKey(ngram)) {
                    freq.put(ngram, freq.get(ngram) + 1);
                } else {
                    freq.put(ngram, 0);
                }
            }
        });

        final int total = freq.entrySet().stream()
                .map(Map.Entry::getValue)
                .reduce((i1, i2) -> i1 + i2)
                .orElseThrow(() -> new RuntimeException("Failed to calculate total. NaN"));

        return freq.entrySet().stream()
                .map(e -> new SimpleEntry<>(e.getKey(), (double) e.getValue() / total * 100))
                .collect(Collectors.toMap(SimpleEntry::getKey, SimpleEntry::getValue));
    }
}
