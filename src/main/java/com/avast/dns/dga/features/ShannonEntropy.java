package com.avast.dns.dga.features;

import java.util.HashMap;
import java.util.Map;

public class ShannonEntropy {

    public static double entropy(final String s) {
        int n = 0;
        final Map<Character, Integer> occ = new HashMap<>();

        for (int c = 0; c < s.length(); ++c) {
            char cx = s.charAt(c);
            if (occ.containsKey(cx)) {
                occ.put(cx, occ.get(cx) + 1);
            } else {
                occ.put(cx, 1);
            }
            ++n;
        }

        double e = 0.0;
        for (Map.Entry<Character, Integer> entry : occ.entrySet()) {
            double p = (double) entry.getValue() / n;
            e += p * log2(p);
        }
        return -e;
    }

    private static double log2(double a) {
        return Math.log(a) / Math.log(2);
    }
}
