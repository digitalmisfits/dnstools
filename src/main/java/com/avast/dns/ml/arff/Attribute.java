package com.avast.dns.ml.arff;

public interface Attribute {

    String toAscii();
}
