package com.avast.dns.ml.arff;

import java.util.Arrays;
import java.util.stream.Collectors;

public class NominalAttributeHeader<T extends Enum<T>> implements AttributeHeader {

    private final String name;
    private final Class<T> classes;

    public NominalAttributeHeader(final String name, final Class<T> classes) {
        this.name = name;
        this.classes = classes;
    }

    @Override
    public String prologue() {

        final String literal = Arrays.stream(classes.getEnumConstants())
                .map(Enum::name)
                .collect(Collectors.joining(", "));

        return String.format("@ATTRIBUTE %s { %s }", name, literal);
    }
}
