package com.avast.dns.ml.arff;

public class RealAttributeHeader implements AttributeHeader {

    private final String name;

    public RealAttributeHeader(final String name) {
        this.name = name;
    }

    @Override
    public String prologue() {
        return String.format("@ATTRIBUTE %s NUMERIC", this.name);
    }
}
