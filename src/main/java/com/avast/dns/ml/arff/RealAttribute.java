package com.avast.dns.ml.arff;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class RealAttribute implements Attribute {

    private DecimalFormat decimal = new DecimalFormat("0.00##", DecimalFormatSymbols.getInstance(Locale.ENGLISH));

    private final double value;

    public RealAttribute(final double value) {
        this.value = value;
    }

    @Override
    public String toAscii() {
        return String.format("%s", decimal.format(value));
    }
}
