package com.avast.dns.ml.arff;

public class RelationHeader {

    private final String relation;

    public RelationHeader(final String relation) {

        this.relation = relation;
    }

    public String prologue() {
        return String.format("@RELATION %s", relation);
    }
}
