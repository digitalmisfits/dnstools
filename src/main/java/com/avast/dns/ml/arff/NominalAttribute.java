package com.avast.dns.ml.arff;

public class NominalAttribute<T extends Enum<T>> implements Attribute {

    private final Enum<T> cls;

    public NominalAttribute(final Enum<T> cls) {
        this.cls = cls;
    }

    @Override
    public String toAscii() {
        return cls.name();
    }
}
