package com.avast.dns.ml;

import com.avast.dns.ml.arff.Attribute;
import com.avast.dns.ml.arff.AttributeHeader;
import com.avast.dns.ml.arff.RelationHeader;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.stream.Collectors;

public class ArffWriter {

    private final Writer writer;

    public ArffWriter(final Writer writer) throws FileNotFoundException {
        this.writer = writer;
    }

    public void write(final RelationHeader relationHeader, final List<AttributeHeader> attributeHeaders,
               final List<List<Attribute>> instances) throws IOException {

        writePrologue(relationHeader, attributeHeaders);
        writeInstances(instances);
    }

    private void writePrologue(final RelationHeader relationHeader, final List<AttributeHeader> attributeHeaders) throws IOException {

        write(relationHeader.prologue());
        attributeHeaders.forEach(attribute -> {
            write(attribute.prologue());
        });
    }

    private void writeInstances(final List<List<Attribute>> instances) {
        write("@DATA");
        instances.forEach(instance -> {
            write(instance.stream().map(Attribute::toAscii).collect(Collectors.joining(",")));
        });
    }

    private void write(final String s) {
        try {
            writer.append(s);
            writer.append("\n");
        } catch (IOException e) {
            throw new RuntimeException("IO exception thrown while writing.", e);
        }
    }

    public void close() throws IOException {
        writer.close();
    }
}
